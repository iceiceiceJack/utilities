#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>

using namespace std;

rs2_stream
find_stream_to_align(const std::vector<rs2::stream_profile> &streams) {
  rs2_stream align_to = RS2_STREAM_ANY;
  bool depth_stream_found = false;
  bool color_stream_found = false;
  for (const rs2::stream_profile &sp : streams) {
    rs2_stream profile_stream = sp.stream_type();
    if (profile_stream != RS2_STREAM_DEPTH) {
      if (!color_stream_found) // Prefer color
        align_to = profile_stream;

      if (profile_stream == RS2_STREAM_COLOR) {
        color_stream_found = true;
      }
    } else {
      depth_stream_found = true;
    }
  }

  if (!depth_stream_found)
    throw std::runtime_error("No Depth stream available");

  if (align_to == RS2_STREAM_ANY)
    throw std::runtime_error("No stream found to align with Depth");

  return align_to;
}


int main(int argc, char *argv[]) {
  rs2::pipeline pipe;
  rs2::config cfg;
  cfg.enable_device(argv[1]);
  cfg.enable_stream(RS2_STREAM_COLOR, 1280, 720, RS2_FORMAT_BGR8, 30);
  cfg.enable_stream(RS2_STREAM_DEPTH, 640, 360, RS2_FORMAT_Z16, 30);

  auto profile = pipe.start(cfg);
  auto align_to = find_stream_to_align(profile.get_streams());
  rs2::align align(align_to);

  for (auto i = 0; i < 30; ++i) {
    pipe.wait_for_frames();
  }
  auto tick_last = cv::getTickCount() / cv::getTickFrequency();
  int count = 0;
  while(true) {
    auto frameset = pipe.wait_for_frames();
    auto processed = align.process(frameset);
    rs2::video_frame other_frame = processed.first(align_to);
    auto color = cv::Mat(other_frame.get_height(), other_frame.get_width(),
                         CV_8UC3, (void *)other_frame.get_data());
    rs2::depth_frame aligned_depth_frame = processed.get_depth_frame();
    auto depth = cv::Mat(aligned_depth_frame.get_height(),
                         aligned_depth_frame.get_width(), CV_16UC1,
                         (void *)aligned_depth_frame.get_data());
    depth.convertTo(depth, CV_8U, 255.0 / 4000);

    if (count++ >= 100) {
      // cv::imwrite("rgb.jpg", color);
      // cv::imwrite("depth.png", depth);

      auto tick = cv::getTickCount() / cv::getTickFrequency();
      cout << 100 / (tick - tick_last) << endl;
      count = 0;
      tick_last = tick;
    }
  }

  return 0;
}

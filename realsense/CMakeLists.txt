aux_source_directory(. REALSENSE)

include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(test_realsense ${REALSENSE})
target_link_libraries(test_realsense realsense2 ${OpenCV_LIBRARIES})

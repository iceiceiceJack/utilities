#include "json.hpp"
#include <iostream>
#include <string>

using json = nlohmann::json;
using namespace std;

int main(int argc, char *argv[])
{
  string text = "{\"keys\": [\"P01_1544608190640\", \"P01_1544608190683\", \"P01_1544608190729\", \"P01_1544608190762\", \"P01_1544608190808\"]}";
  // cout << text << endl;

  try {
    // auto res = json::parse(text);
    // cout << setw(2) << res << endl;

    // vector<string> keys;
    // if (res["keys"].empty())
    //   cout << "keys empty";
    // else
    //   cout << res["keys"] << endl;
    // for (auto &iter : res["keys"]) {
    //   cout << iter << endl;
    // }
    vector<string> names = {"hand", "head"};
    json result;
    result["head"] = json::array();
    result["hand"] = json::array();
    result[names[1]].push_back({1,2,3,4});
    string rect_string = result.dump();
    cout << result.dump() << endl;
    json resp;
    for (int i = 0; i < 2; ++i) {
      json ele;
      ele["key"] = "aaaaaa";
      ele["rect"] = json::parse(rect_string);
      resp.push_back(ele);
    }
    cout << resp << endl;
  } catch (json::exception &e) {
    cerr << e.id << endl;
    cerr << e.what() << endl;
  }

  return 0;
}

#include "redis_wrapper.hpp"
#include <iostream>
#include <memory>
#include <thread>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

shared_ptr<Redis> redis;

void redis_process() {
  auto mat = imread("/Users/jackie/rgb.jpg", CV_LOAD_IMAGE_COLOR);
  vector<char> buffer;
  vector<uchar> buf;
  imencode(".jpg", mat, buf);

  for (int i = 0; i < buf.size(); ++i) {
    buffer.push_back(buf[i]);
  }

  for (int i = 0; i < 9999; ++i) {
    if (redis->Set("rgb" + to_string(i), buffer, 10))
      cout << i << endl;
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  for (int i = 0; i < 1; ++i) {
    if (redis->Set("rgb" + to_string(i), buffer, 10))
      cout << i << endl;
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  for (int i = 0; i < 1; ++i) {
    if (redis->Set("rgb" + to_string(i), buffer, 10))
      cout << i << endl;
  }
}


int main(int argc, char *argv[]) {
  redis = make_shared<Redis>("10.2.0.132");

  redis->Connect();
  // redis->SelectDB(1);

  vector<thread> threads;
  for (int i = 0; i < 10; ++i) {
    threads.emplace_back(redis_process);
  }

  for (auto &thread : threads) {
    thread.join();
  }

  return 0;
}

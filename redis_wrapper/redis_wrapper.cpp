#include "redis_wrapper.hpp"
#include <chrono>
#include <thread>
#include <iostream>

using namespace std;

Redis::Redis(const string &host, const int port, const int timeout):
  host_(host),
  port_(port) {
  timeout_.tv_sec = timeout / 1000;
  timeout_.tv_usec = (timeout % 1000) * 1000;
}


Redis::~Redis() {
  if (context_ != nullptr)
    redisFree(context_);
}


bool Redis::Connect() {
  lock_guard<mutex> lock(redis_mutex_);

  if (context_ != nullptr) {
    redisFree(context_);
  }
  context_ = redisConnectWithTimeout(host_.c_str(), port_, timeout_);

  if (context_ == nullptr) {
    cerr << "Connection error: can't allocate redis context" << endl;
    return false;
  } else if (context_->err != 0) {
    cerr << "Connection error: " << context_->errstr << endl;
    redisFree(context_);
    return false;
  }

  return true;
}


bool Redis::SelectDB(const int id) {
  redisReply * reply = nullptr;

  {
    lock_guard<mutex> lock(redis_mutex_);
    reply = (redisReply *)redisCommand(context_, "SELECT %d", id);
  }

  if (reply != nullptr) {
    switch (reply->type) {
      case REDIS_REPLY_STATUS:
        freeReplyObject(reply);
        return true;

      default:
        cerr << "redis select db: " << reply->type << endl << reply->str << endl;
        freeReplyObject(reply);
        return false;
    }
  }

  if (!try_reconnecting_) {
    std::thread reconnect(&Redis::Reconnect, this);
    reconnect.detach();
  }

  return false;
}


bool Redis::Set(const string &key, const vector<char> &value, const int ttl) {
  redisReply * reply = nullptr;

  {
    lock_guard<mutex> lock(redis_mutex_);
    reply = (redisReply*)redisCommand(context_, "SETEX %s %d %b", key.c_str(), ttl, &value[0], value.size());
  }

  if (reply != nullptr) {
    switch (reply->type) {
      case REDIS_REPLY_STATUS:
        freeReplyObject(reply);
        return true;

      default:
        cerr << "redis set: " << reply->type << endl << reply->str << endl;
        freeReplyObject(reply);
        return false;
    }
  }

  if (!try_reconnecting_) {
    std::thread reconnect(&Redis::Reconnect, this);
    reconnect.detach();
  }

  return false;
}


vector<char> Redis::Get(const string &key) {
  redisReply * reply = nullptr;
  vector<char> result;

  {
    lock_guard<mutex> lock(redis_mutex_);
    reply = (redisReply*)redisCommand(context_, "GET %s", key.c_str());
  }

  if (reply != nullptr) {
    switch (reply->type) {
      case REDIS_REPLY_STRING:
        result.insert(result.begin(), reply->str, reply->str + reply->len);
        freeReplyObject(reply);
        return result;

      default:
        cerr << "redis get: " << reply->type << endl << reply->str << endl;
        freeReplyObject(reply);
        return result;
    }
  }

  if (!try_reconnecting_) {
    std::thread reconnect(&Redis::Reconnect, this);
    reconnect.detach();
  }

  return result;
}


unordered_map<string, vector<char>> Redis::MGet(const vector<string> &keys) {
  redisReply * reply = nullptr;
  unordered_map<string, vector<char>> result;

  if (keys.empty()) return result;

  string cmd;
  cmd += "MGET ";
  for (auto iter = keys.begin(); iter != keys.end(); ++iter) {
    cmd += *iter;
    cmd += " ";
  }
  {
    lock_guard<mutex> lock(redis_mutex_);
    reply = (redisReply*)redisCommand(context_, cmd.c_str());
  }

  switch (reply->type) {
    case REDIS_REPLY_ARRAY: 
      for (int i = 0; i < reply->elements; ++i) {
        auto child_reply = (redisReply *)reply->element[i];
        switch (child_reply->type) {
          case REDIS_REPLY_STRING:
            result.insert(make_pair(keys[i],
                                    vector<char>(child_reply->str,
                                                 child_reply->str + child_reply->len)));
            break;

          default:
            break;
        }
      }
      freeReplyObject(reply);
      return result;

    default:
      cerr << "redis mget: " << reply->type << endl << reply->str << endl;
      freeReplyObject(reply);
      return result;
  }

  if (!try_reconnecting_) {
    std::thread reconnect(&Redis::Reconnect, this);
    reconnect.detach();
  }

  return result;
}


void Redis::Reconnect() {
  lock_guard<mutex> lock(reconnect_mutex_);
  try_reconnecting_ = true;
  cerr << "redis will reconnect soon!!" << endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  {
    lock_guard<mutex> lock(redis_mutex_);
    redisReconnect(context_);
  }
  try_reconnecting_ = false;
}

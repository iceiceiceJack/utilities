#pragma once

#include <hiredis/hiredis.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <mutex>

class Redis
{
public:
  Redis(const std::string &host = "127.0.0.1", const int port = 6379, const int timeout = 1500);
  ~Redis();

  bool Connect();
  bool SelectDB(const int id);
  bool Set(const std::string &key, const std::vector<char> &value, const int ttl);
  std::vector<char> Get(const std::string &key);
  std::unordered_map<std::string, std::vector<char>> MGet(const std::vector<std::string> &keys);

private:
  void Reconnect();

private:
  const std::string host_;
  const int port_;
  struct timeval timeout_;
  bool try_reconnecting_ = false;

  redisContext* context_ = nullptr;
  std::mutex redis_mutex_;
  std::mutex reconnect_mutex_;
};

